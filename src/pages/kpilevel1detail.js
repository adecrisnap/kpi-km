import React, { useEffect, useState } from 'react';
import { 
    Container,
    Grid,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import LoadingBox2 from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import usePostData from '../services/usePostData';
import usePutData from '../services/usePutData';
import SimpleForm from '../components/SimpleForm'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import { useHistory } from "react-router-dom";
import Helper from '../services/Helper'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Cookies from 'js-cookie';


const KPILevel1Detail = props => {
    const role_id = Cookies.get('role_id')
    let history = useHistory();
    const {
        id,
        mode
    } = props.match.params
    const [ openload, setOpenLoad] = useState(false)
    const [ idstate, setIDState ] = useState(id);
    const [ modestate, setModeState ] = useState(mode);
    const [elements,setElements] = useState([]);
    const [action,setAction] = useState('get');
    const  { stateGet, setGetUrl } = useGetData(
        null,''
    );
    const  { statePost, setPostUrl, setPayload } = usePostData(
        null, null,''
    );
    const  { statePut, setPutUrl, setPutload } = usePutData(
        null, null,''
    );

    useEffect(()=>{
        if (modestate==='edit') {
            setAction('get');
            setGetUrl(URLLocation.getUrl() + '/getkpigroup1byid/' + idstate,'')
        }
        else {
           var val = [
            {   value : ''  },
            {   value : ''  },
            {   value : ''  },
            {   value : 1   },
            {   value : new Date() }
           ]
           fillElements(val);
        }
    },[])

    useEffect(()=>{
        if (checkStatus()===true)
        {
            var idtemp = id;   
            var group_code = null;
            var group_name = null;
            var sequence = null;
            var is_active = null;
            var lastmodified = null;
            
            if (action==='get') {
                group_code = stateGet.data[0].group_code;
                group_name = stateGet.data[0].group_name; 
                sequence = stateGet.data[0].sequence; 
                is_active = stateGet.data[0].is_active;
                lastmodified = stateGet.data[0].lastmodified;
            }
            if (action==='post') {
                setModeState('edit');
                idtemp = statePost.data.id;
            }
            if (action==='put' || action==='post') {
                group_code = elements[0].value
                group_name = elements[1].value
                sequence = elements[2].value
                is_active = elements[3].value
                lastmodified = elements[4].value
            }
           
            var val = [
                {   value : group_code  },
                {   value : group_name  },
                {   value : sequence  },
                {   value : is_active  },
                {   value : lastmodified  },
            ]

            setIDState(idtemp);
            fillElements(val);
            setSuccessMessage();
        }
        else {
            setErrorMessage();
        }
    },[stateGet.randomstatus,statePost.randomstatus,statePut.randomstatus])

    const fillElements = (val) =>{
        var elem = [];
        elem.push({
            type:'text',
            name:'group_code',
            label:'Code',
            placeholder:'Code',
            value:val[0].value, 
        })

        elem.push({
            type:'text',
            name:'group_name',
            label:'KPI Name',
            placeholder:'KPI Name',
            value:val[1].value, 
        })

        elem.push({
            type:'text',
            name:'sequence',
            label:'Sequence',
            placeholder:'Sequence',
            value:val[2].value, 
        })

        elem.push({
            type:'checkbox',
            name:'is_active',
            label:'Is Active',
            value:val[3].value, 
        })

        elem.push({
            type:'label',
            datatype:'DATETIME',
            name:'lastmodified',
            label:'Last Modified',
            value:val[4].value, 
        })

        setElements(elem);
    }

    const onYes = () => {
        var data = {
            group_code : elements[0].value,
            group_name : elements[1].value,
            sequence : elements[2].value,
            is_active : elements[3].value,
            roleid : role_id
        };
        operationToServer(data,'/editkpigroup1/' + idstate,'/createkpigroup1')
    }

    //REGION FIXED OPERATIONS
    const operationToServer = (data,editUrl,addUrl) => {
        if (modestate==='edit') {
            setPutload(data);
            setAction('put')
            setPutUrl(URLLocation.getUrl() + editUrl);
        }
        else {
            setPayload(data);
            setAction('post');
            setPostUrl(URLLocation.getUrl() + addUrl);
        }
    }

    const setSuccessMessage = () => {
        if (action==='put' || action==='post') NotificationManager.success('Data saved', 'Success', 3000);
    }

    const setErrorMessage = () => {
        var msg = '';
        if (action==='get') msg = stateGet.errorMessage;
        if (action==='put') msg = statePut.errorMessage;
        if (action==='post') msg = statePost.errorMessage;
        if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
    }

    const checkStatus = () => {
        if  ((action==='get' && (stateGet.status===200 || stateGet.status===201 || stateGet.status===304)) ||
            (action==='post' && (statePost.status===200 || statePost.status===201)) ||
            (action==='put' && (statePut.status===200 || statePut.status===201))
        )
            return true
        else
            return false
    }

    const onClose = () => {
        history.goBack();
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    const onDateChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].selectedDate = value;
        setElements(elems);
    }


    return (
        <Container>
            <HeaderEnhanced title='KPI Level 1' text='Pengelompokkan KPI Level 1' />
            <LoadingBox2 open={openload}/>
            <NotificationContainer/>
            {(stateGet.isLoading===true || statePost.isLoading===true || statePut.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <SimpleForm
                                    elements={elements}
                                    yesCaption='Save'
                                    noCaption='Cancel'
                                    numberofcolumns={2}
                                    onClose={onClose}
                                    onChange={onChange}
                                    onCheckChange={onCheckChange}
                                    onYes={onYes}
                                    label={''}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>            
                )
            }
        </Container>
    )
}

export default KPILevel1Detail;
