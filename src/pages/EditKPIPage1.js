import React, { useEffect, useState } from 'react'
import { 
    Container,
    Grid,
    Dropdown,
    Button,
    Segment,
    Header
} from 'semantic-ui-react'
import LoadingBox from '../components/LoadingBox'
import LoadingBox2 from '../components/LoadingBox'
import URLLocation from '../services/URLLocation'
import useGetData from '../services/useGetData';
import SimpleForm from '../components/SimpleForm'
import usePostData from '../services/usePostData';
import HeaderEnhanced from '../components/HeaderEnhanced'
import Helper from '../services/Helper'
//import { useHistory } from "react-router-dom"
import {NotificationContainer, NotificationManager} from 'react-notifications'
import 'react-notifications/lib/notifications.css'
import EmptySpace from '../components/EmptySpace'
import Cookies from 'js-cookie'

const EditKPIPage1 = props => {
    const role_id = Cookies.get('role_id')
    const  { stateGet,setGetUrl, setRefresh } = useGetData(
        null,''
    );
    const { statePost, setPayload, setPostUrl } = usePostData(URLLocation.getUrl() + '/getcurrentscores',null,'')
    const [ elements,setElements] = useState(null);
    const [openload,setOpenLoad] = useState(false)
    const [ tahun,setTahun] = useState(null)
    const [ bulan, setBulan] = useState(null)
    const [ plant, setPlant] = useState(null)
    const [ level, setLevel] = useState(null)
    const [ KPI, setKPI] = useState(null)
    const [ screen, setScreen] = useState(1)
    const [ saveScores, setSaveScores] = useState(0)
    const [ plantOptions, setPlantOptions] = useState(null)
    const [ KPIOptions, setKPIOptions] = useState(null)
    const [ yearOptions, setYearOptions ] = useState(null)
    
    
    /*
        const yearOptions = [
            {
            text: 2021,
            value: 2021,
            },
            {
            text: 2022,
            value: 2022,
            },
            {
            text: 2023,
            value: 2023,
            },
            {
            text: 2024,
            value: 2024,
            },
            {
            text: 2025,
            value: 2025,
            },
            {
            text: 2026,
            value: 2026,
            },
            {
                text: 2027,
                value: 2027,
            },
            {
                text: 2028,
                value: 2028,
            },
            {
                text: 2029,
                value: 2029,
            },
            {
                text: 2030,
                value: 2030,
            },
        ]    
    */
    const levelOptions = [
        { 
            text : 'KPI Level 2',
            value : 2
        },
        { 
            text : 'KPI Level 3',
            value : 3
        }
    ]

    const translateMonth = (mth)=>{
        if (mth===1) return 'Januari'
        if (mth===2) return 'Februari'
        if (mth===3) return 'Maret'
        if (mth===4) return 'April'
        if (mth===5) return 'Mei'
        if (mth===6) return 'Juni'
        if (mth===7) return 'Juli'
        if (mth===8) return 'Agustus'
        if (mth===9) return 'September'
        if (mth===10) return 'Oktober'
        if (mth===11) return 'November'
        if (mth===12) return 'Desember'
      
    }

    var monthOptions = []
    const is_admin = Cookies.get('is_admin')
    if (is_admin==='1') {
        monthOptions = [
            {
                text : 'Januari',
                value : 1
            },
            {
                text : 'Februari',
                value : 2
            },
            {
                text : 'Maret',
                value : 3
            },
            {
                text : 'April',
                value : 4
            },
            {
                text : 'Mei',
                value : 5
            },
            {
                text : 'Juni',
                value : 6
            },
            {
                text : 'Juli',
                value : 7
            },
            {
                text : 'Agustus',
                value : 8
            },
            {
                text : 'September',
                value : 9
            },
            {
                text : 'Oktober',
                value : 10
            },
            {
                text : 'November',
                value : 11
            },
            {
                text : 'Desember',
                value : 12
            },
        ]
       
    }
    else {
        /* var mth = new Date().getMonth()
        if (mth===1) {
            monthOptions.push({
                text : 'Desember',
                value : 12
            })
        }
        else {
            var mthtemp = mth
            const mthtext = translateMonth(mthtemp)
            monthOptions.push({
                text : mthtext,
                value : mthtemp
            })
        } */

        monthOptions = [
            {
                text : 'Januari',
                value : 1
            },
            {
                text : 'Februari',
                value : 2
            },
            {
                text : 'Maret',
                value : 3
            },
            {
                text : 'April',
                value : 4
            },
            {
                text : 'Mei',
                value : 5
            },
            {
                text : 'Juni',
                value : 6
            },
            {
                text : 'Juli',
                value : 7
            },
            {
                text : 'Agustus',
                value : 8
            },
            {
                text : 'September',
                value : 9
            },
            {
                text : 'Oktober',
                value : 10
            },
            {
                text : 'November',
                value : 11
            },
            {
                text : 'Desember',
                value : 12
            },
        ]
    }

  

    const dropdownChange = (event, {value} ) => {
        setTahun(value)
    }

    const dropdownChange4 = (event, {value} ) => {
        setBulan(value)
    }

    const dropdownChange2 = (event, {value} ) => {
        setPlant(value)
    }

    const dropdownChange3 = (event, {value} ) => {
        setLevel(value)
    }

    const dropdownChange5 = (event, {value} ) => {
        setKPI(value)
    }

    const onClose = () => {
        //history.goBack();
        setScreen(1)
    } 

    const onChange = (e,{value}) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',e.target.name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onDropdownChange = (name,value) => {
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = value;
        setElements(elems);
    }

    const onCheckChange = (name,value) => {
        var val = 1;
        if (value===1) val = 0;
        
        var elems = [...elements];
        var idx = Helper.findIndex(elems,'name',name);
        elems[idx].value = val;
        setElements(elems);
    }

    useEffect(()=>{
        var y1 = new Date().getFullYear()
        var y0 = y1 - 1;
        const yopt = [
            {
                text: y0,
                value: y0,
            },
            {
                text: y1,
                value: y1,
            },
        ]
        setYearOptions(yopt)
        var options = []
        async function fetchAPI() {
            const hoplant = Cookies.get('hoplant')
            if (hoplant==='2') {
                setOpenLoad(true)
                const apiUrl = URLLocation.getUrl() + '/getplants';
                let response = await fetch(apiUrl)
                response = await response.json()
                response.map((obj)=>{ 
                    options.push({
                        value :obj.name_coy,
                        text : obj.name_coy 
                    })
                })
            }
            else {
                options.push({
                    value :'HO',
                    text : 'HO' 
                })
            }
            setPlant('HO')
            setPlantOptions(options)
            setOpenLoad(false)
        }
        fetchAPI()
        //setTahun(new Date().getFullYear())
        //setBulan(new Date().getMonth() + 1)
        var dt = new Date()
        if (dt.getMonth()===1) {
            setBulan(12)
            setTahun(new Date().getFullYear()-1)
        }
        else {
            setBulan(dt.getMonth())
            setTahun(new Date().getFullYear())
        }

    },[])

    useEffect(()=>{
        if (level!==null) {
            setGetUrl(URLLocation.getUrl() + '/getkpisinlevel/' + level + '/' + role_id)
            setRefresh(true)
        }
    },[level])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201) {
            var data = []
            for(var i=0;i<stateGet.data.length;i++) {
                data.push({
                    text : stateGet.data[i].group_name,
                    value : stateGet.data[i].id
                })
            }
            setKPIOptions(data)
        }
        else {
            if (stateGet.status!==0) NotificationManager.error('Error','Error',3000)
        }
    },[stateGet.randomstatus])

    useEffect(()=>{
        if (statePost.status===200 || statePost.status===201) {
            if (saveScores===0) {
                var elem = []
                for(var i=0;i<statePost.data.length;i++) {
                    elem.push({
                        type: 'text',
                        name: statePost.data[i].idkpi+ '-1',
                        label: statePost.data[i].group_name + ' (MTD). TARGET : ' + statePost.data[i].target_mtd + ' ' + statePost.data[i].uom,
                        placeholder:'(MTD)',
                        //disabled : (statePost.data[i].ytd_measurement===3) ? false : true,
                        value:statePost.data[i].actual_mtd, 
                    })
                    elem.push({
                        type: 'text',
                        name: statePost.data[i].idkpi +'-2',
                        label: statePost.data[i].group_name + ' (YTD). TARGET : ' + statePost.data[i].target_ytd + ' ' + statePost.data[i].uom,
                        placeholder:'(YTD)',
                        visible : (statePost.data[i].ytd_measurement===3) ? true : false,
                        value:statePost.data[i].actual_ytd, 
                    })
                }
                setElements(elem)
            }
            else {
        
                setSaveScores(0)
                NotificationManager.success('Success','Success',3000)
                setElements(null)
                setTahun(null)
                setBulan(null)
                setPlant(null)
                setLevel(null)
                setKPI(null)
                setScreen(1)
            }
        }
    },[statePost.randomstatus])

    const setFilter = () => {
        if (tahun===null || bulan===null || plant===null || level===null || KPI===null) {
            NotificationManager.error('Parameter belum diisi lengkap','Error', 3000)
        }
        else {
            setScreen(2)
            var data = {
                tahun : tahun,
                bulan : bulan,
                plant : plant,
                level : level,
                kpi : KPI
            }
            setPostUrl(URLLocation.getUrl() + '/getcurrentscores')
            setPayload(data)
        }
    }

    const fillElements = (val) =>{
    }
    
    const onYes = () => {
        var kpis = []
        for(var i=0;i<elements.length;i++){
            kpis.push({
                kpiid : elements[i].name,
                value : elements[i].value
            })
        }
        var data = {
            plant : plant,
            tahun : tahun,
            bulan : bulan,
            level : level,
            kpis : kpis
        }
        setSaveScores(1)
        setPostUrl(URLLocation.getUrl() + '/editscoreskpis')
        setPayload(data)
    }

    //REGION FIXED OPERATIONS
    const operationToServer = (data,editUrl,addUrl) => {
    }

    const setSuccessMessage = () => {
    }

    const setErrorMessage = () => {
    }

    const convertBulan = (val) =>{
        var ret = ''
        if (val===1) ret = 'Januari'
        if (val===2) ret = 'Februari'
        if (val===3) ret = 'Maret'
        if (val===4) ret = 'April'
        if (val===5) ret = 'Mei'
        if (val===6) ret = 'Juni'
        if (val===7) ret = 'Juli'
        if (val===8) ret = 'Agustus'
        if (val===9) ret = 'September'
        if (val===10) ret = 'Oktober'
        if (val===11) ret = 'November'
        if (val===12) ret = 'Desember'
        return ret
    }

    return (
        <Container>
            <HeaderEnhanced title='Edit Capaian KPI' text='Untuk mengubah secara manual capaian KPI' />
            <NotificationContainer/>
            <LoadingBox2 open={openload}/>
            {(screen===1) ? 
            (
                <Segment textAlign='left' style={{ marginLeft: "2.5em", marginRight: "2.5em", marginTop: "2em"}} raised>
                    {(stateGet.isLoading===true) ?
                        (
                            <LoadingBox open={true}/>
                        )
                        :
                        (
                            <>
                                <Grid stackable>
                                    <Grid.Row>
                                        <Grid.Column>
                                            <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                                                Set Filter
                                                <Header.Subheader>Isi parameter untuk memilih KPI yang akan diedit capaiannya</Header.Subheader>
                                            </Header>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            <Grid columns={2}>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <label>Pilih Organisasi</label>
                                                        <Dropdown
                                                            placeholder='Pilih Organisasi'
                                                            selection
                                                            fluid
                                                            value={plant}
                                                            onChange={dropdownChange2}
                                                            options={plantOptions}
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <label>Pilih Tahun</label>
                                                        <Dropdown
                                                            placeholder='Pilih Tahun'
                                                            selection
                                                            fluid
                                                            value={tahun}
                                                            onChange={dropdownChange}
                                                            options={yearOptions}
                                                        />
                                                    </Grid.Column>
                                                </Grid.Row>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <label>Pilih Bulan</label>
                                                        <Dropdown
                                                            placeholder='Pilih Bulan'
                                                            selection
                                                            fluid
                                                            value={bulan}
                                                            onChange={dropdownChange4}
                                                            options={monthOptions}
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <label>Pilih Level</label>
                                                        <Dropdown
                                                            placeholder='Pilih Level'
                                                            selection
                                                            fluid
                                                            value={level}
                                                            onChange={dropdownChange3}
                                                            options={levelOptions}
                                                        />
                                                    </Grid.Column>
                                                </Grid.Row>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <label>Pilih KPI</label>
                                                        <Dropdown
                                                            placeholder='Pilih KPI'
                                                            onChange={dropdownChange5}
                                                            options={KPIOptions}
                                                            selection
                                                            fluid
                                                            search
                                                            value={KPI}
                                                            multiple
                                                        />
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <Button positive onClick={setFilter}>Edit Capaian</Button>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid> 
                                &nbsp;
                                <p/>    
                            </>     
                        )
                    }
                </Segment>
            )
            : 
            (
                (statePost.isLoading===true) ?
                    (
                        <LoadingBox open={true}/>
                    ) 
                    :
                    (
                        <Grid>
                            <Grid.Row>
                                <Grid.Column>
                                    {(elements!==null) ? (
                                        (elements.length>0) ? 
                                        (
                                            <SimpleForm
                                                elements={elements}
                                                yesCaption='Save'
                                                noCaption='Cancel'
                                                numberofcolumns={2}
                                                onClose={onClose}
                                                onChange={onChange}
                                                onCheckChange={onCheckChange}
                                                onYes={onYes}
                                                label={'Periode ' + convertBulan(bulan) + ' ' + tahun}
                                                sublabel={'NOTE : Hanya KPI dengan YTD Measurement tipe Adjustment yang nilai YTD-nya bisa diisi manual'}
                                            />
                                        )
                                        :
                                        (
                                            <EmptySpace
                                                title={'Belum ada setting target'}
                                                caption={'Set target dahulu untuk kombinasi organisasi, tahun, bulan, dan level tersebut'}
                                                showButton={true}
                                                image={null}
                                                buttonCaption={'Ke halaman Target'}
                                                link={'target'}
                                            />
                                        )
                                    )
                                    :
                                    (
                                        <EmptySpace
                                            title={'Belum ada setting target'}
                                            caption={'Set target dahulu untuk kombinasi organisasi, tahun, bulan, dan level tersebut'}
                                            showButton={true}
                                            image={null}
                                            buttonCaption={'Ke halaman Target'}
                                            link={'target'}
                                        />
                                    )
                                    }
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>     
                    )
                     
            )}
        </Container>
    )
}

export default EditKPIPage1

/*
<SimpleForm
                                elements={elements}
                                yesCaption='Save'
                                noCaption='Cancel'
                                numberofcolumns={2}
                                onClose={onClose}
                                onChange={onChange}
                                onDropdownChange={onDropdownChange}
                                onCheckChange={onCheckChange}
                                onYes={onYes}
                                label={'Edit score KPI'}
                            />
                            */
