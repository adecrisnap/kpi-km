import React from 'react'
import { AuthContext } from './AuthContext';

function useAuthContext() {
    return React.useContext(AuthContext);
}

export default useAuthContext;