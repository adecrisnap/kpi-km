import React, {useEffect} from 'react';
import {
    Modal,
    Button,
    Form
} from 'semantic-ui-react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


const SimpleModalForm = props => {
    const {
        title,
        elements,
        size,
        yesCaption,
        noCaption
    } = props
    const components = null;

    useEffect(()=>{
        var compx = [];
        for(var i=0;i<elements.length;i++) {
            switch(elements[i].type) {
                case "text":
                    compx.push(
                        <Form.Input 
                            name={elements[i].name}
                            fluid label={elements[i].label} 
                            placeholder={elements[i].placeholder}
                            onChange={props.onChange}
                            value={elements[i].value}
                        />
                    )
                    break;
                case "memo":
                    compx.push(
                        <Form.TextArea 
                            name={elements[i].name}
                            label={elements[i].label} 
                            placeholder={elements[i].placeholder} 
                            onChange={props.onChange}
                            value={elements[i].value}
                        />)
                    break;
                case "checkbox":
                    compx.push(
                        <Form.Checkbox 
                            label={elements[i].label}
                            name={elements[i].name}
                            value={elements[i].value}
                            //toggle
                            checked={((elements[i].value===1) ? true : false)}
                            onChange={props.onCheckChange}
                        />
                    )
                    break;
                case "password":
                    compx.push(
                        <Form.Input 
                            name={elements[i].name}
                            fluid label={elements[i].label} 
                            placeholder={elements[i].placeholder}
                            type='password'
                            onChange={props.onChange}
                            value={elements[i].value}
                        />
                    )
                    break;
                case "calendar":
                    compx.push(
                        <DatePicker 
                            name={elements[i].name}
                            selected={elements[i].selectedDate} 
                            onChange={props.onDateChange}
                        />)
                    break;    
                case "dropdown":
                    compx.push(
                    <Form.Dropdown
                        name={elements[i].name}
                        label={elements[i].label}
                        fluid
                        onChange={props.onDropdownChange}
                        options={elements[i].options}
                        selection
                        multiple={elements[i].multiple}
                        value={elements[i].value}
                    />)
                    break;
            }
        }
        compx.push(<Form.Group>{compx}</Form.Group>)
        components = compx
    },[])

    return (
        <Modal
            size={size}
            onClose={props.onClose}
            open={props.open}
        >
            <Modal.Header>
                {title}
            </Modal.Header>
            <Modal.Content>
                <Modal.Description>
                    {components}
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button color='black' onClick={props.onClose}>
                    {noCaption}
                </Button>
                <Button
                    content={yesCaption}
                    labelPosition='right'
                    icon='checkmark'
                    onClick={props.onYes}
                    positive
                />
            </Modal.Actions>
        </Modal>
    )
}

export default SimpleModalForm