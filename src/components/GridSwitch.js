import React, { useState } from 'react';
import { 
    Grid, 
    Form,
    Button
} from 'semantic-ui-react';
import GridTable from './GridTable';
import GridCard from './GridCard';

const GridSwitch = props => {
    const [ mode, setMode ] = useState(props.showmode);
    const changeLayout = (mode) => {
        setMode(mode)
    }

    return (
        <Grid>
            <Grid.Row>
                <Grid.Column>
                    <Form.Group>
                        <Button icon='grid layout' onClick={changeLayout(1)}/>
                        <Button icon='table' onClick={changeLayout(2)}/>
                    </Form.Group>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column>
                    {(mode===1) ? 
                        (
                            <GridCard {...props}/>
                        ) 
                        : 
                        (
                            <GridTable {...props}/>
                        )
                    }
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default GridSwitch;
/*

const { 
            showmode,

            dataintable, 
            columns,
            rowsperpage,
            linkinrowlevel,
            internalclick,

            dataingrid, 
            recordsperpage,
            columnsnumber,
            optionssortby,

            link,
            sortedby,
            ascdesc,
            showaddnew,
            showaddnewonempty,
            emptycaption,
            emptyimage,
            emptytitle,
            emptybuttoncaption
        } = props

*/