import React from 'react';
import { 
    Header,
} from 'semantic-ui-react';


const HeaderEnhanced = props => {
    return (
 
            <Header as='h3' textAlign='left' style={{ marginLeft: '2.5em' }}>
                <Header.Content>
                    {props.title}
                    <Header.Subheader>{props.text}</Header.Subheader>
                </Header.Content>
            </Header>
      
    )
}

export default HeaderEnhanced;