import React, { useState, useEffect} from 'react';
import { 
  Button, 
  Form, 
  Grid, 
  Header, 
  Icon,  
  Segment,
  Container
} from 'semantic-ui-react';
import LoadingBox from './components/LoadingBox';
import URLLocation from './services/URLLocation';
import usePostData from './services/usePostData';
//import HomeContainer from './HomeContainer';
import Home from './Home';
import useAuthContext from './services/stores/useAuthContext';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { useHistory } from 'react-router-dom'


const Login = () => {
  const history = useHistory()
  const { dispatch } = useAuthContext();
  const  { statePost, setPayload } = usePostData(
    URLLocation.getUrl() + '/loginapp', null, ''
  );
  const [ NIK, setNIK ] = useState('')
  const [ Pwd, setPwd ] = useState('')
  
  const onChange1 = (e) => {
    setNIK(e.target.value)
  }

  const onChange2 = (e) => {
    setPwd(e.target.value)
  }

  const login = () => {
    var data = {
      nik : NIK,
      pwd : Pwd,
      appid : 2,
      nonkmdb : 0,
      isencrypted : false
    }
    setPayload(data)
  }
  
  useEffect(()=>{
    if (statePost.status===200 || statePost.status===201) {
      var payload = {
        id: statePost.data.id,
        partner_name :statePost.data.partner_name,
        menus : statePost.data.menus, 
        rolesfinal : statePost.data.rolesfinal
      }
      dispatch({
          type: "LOGIN",
          payload: payload
      });

      //window.location.href= '/home'
      //history.replace('/home')
    }
    else {
      if (statePost.status!==0) NotificationManager.error('Error', 'Error', 3000);
    }
  },[statePost.randomstatus])

  return (
    <Container>
      <LoadingBox open={statePost.isLoading} />
      <NotificationContainer/>
     
      {(statePost.status===200) ?
        <Home storedData={statePost.data} fromLocal={false}/>
        :
        <>
        <p/>
        <Header as='h2' textAlign='center'>
          <Icon name='user' /> Log-in to your account
        </Header>
        <p/>
        <Segment placeholder>
        <p/>
          <Grid stackable>
            <Grid.Column verticalAlign="middle">
              <Form>
                <Form.Input 
                  key='nik'
                  id='nik'
                  name="nik" 
                  fluid 
                  icon='user' 
                  iconPosition='left' 
                  placeholder='NIK' 
                  value={NIK} 
                  onChange={onChange1} 
                />
                <Form.Input
                  key='passwd'
                  id='passwd'
                  name="passwd"
                  fluid
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password'
                  type='password'
                  value={Pwd}
                  onChange={onChange2}
                  
                />
                <Button positive fluid size='large' onClick={login}>
                  Login
                </Button>
              </Form>
            </Grid.Column>
          </Grid>
        &nbsp;<p/>
      </Segment>
      </>
      }
    </Container>
  );
}

export default Login;

/*

 dispatch({
                type: "LOGIN",
                payload: statePost.data
            })
 
      */

